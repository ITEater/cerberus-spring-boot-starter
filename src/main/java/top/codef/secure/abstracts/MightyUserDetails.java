package top.codef.secure.abstracts;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.core.userdetails.UserDetails;

public abstract class MightyUserDetails implements UserDetails {

	private static final long serialVersionUID = -4194776615963121062L;

	private Map<String, Object> additionalDetail = new HashMap<String, Object>();

	public Map<String, Object> getAdditionalDetail() {
		return additionalDetail;
	}

	public void setAdditionalDetail(Map<String, Object> additionalDetail) {
		this.additionalDetail = additionalDetail;
	}

	public void add(String key, Object detail) {
		additionalDetail.put(key, detail);
	}

	public void remove(String key) {
		additionalDetail.remove(key);
	}

}
