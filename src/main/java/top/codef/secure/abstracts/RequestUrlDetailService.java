package top.codef.secure.abstracts;

import java.util.List;

import top.codef.secure.authority.AuthWithRequestUrl;

@FunctionalInterface
public interface RequestUrlDetailService {

	List<AuthWithRequestUrl> details();

}
