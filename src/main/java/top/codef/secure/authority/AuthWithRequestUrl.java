package top.codef.secure.authority;

import java.util.List;

import org.springframework.beans.BeanUtils;

import top.codef.secure.cloud.entities.AuthInfoWithServiceInterface;
import top.codef.secure.cloud.entities.CodefMightAuth;
import top.codef.secure.requesturl.RequestUrl;

public class AuthWithRequestUrl {

	private MightyAuth auth;

	private List<RequestUrl> requestUrlList;

	public AuthWithRequestUrl(MightyAuth auth, List<RequestUrl> requestUrlList) {
		this.auth = auth;
		this.requestUrlList = requestUrlList;
	}

	public AuthWithRequestUrl(AuthInfoWithServiceInterface authInfoWithServiceInterface) {
		this.auth = new CodefMightAuth();
		BeanUtils.copyProperties(authInfoWithServiceInterface, auth);
		this.requestUrlList = authInfoWithServiceInterface.getInterfaceList();
	}

	public AuthWithRequestUrl() {
	}

	public MightyAuth getAuth() {
		return auth;
	}

	public void setAuth(MightyAuth auth) {
		this.auth = auth;
	}

	public List<RequestUrl> getRequestUrlList() {
		return requestUrlList;
	}

	public void setRequestUrlList(List<RequestUrl> requestUrlList) {
		this.requestUrlList = requestUrlList;
	}

}
