package top.codef.secure.authority;

import org.springframework.security.core.GrantedAuthority;

public interface MightyAuth extends GrantedAuthority {

	String getAuthName();

	@Override
	default String getAuthority() {
		return String.format("ROLE_%s", getAuthName());
	}

}
