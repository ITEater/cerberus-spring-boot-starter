                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        package top.codef.secure.authority;

import java.util.List;

import top.codef.secure.requesturl.RequestUrl;

public interface RequestUrlAuth {

	public RequestUrl getRequestUrl();

	public <T extends MightyAuth> List<T> getAuthorities();

}
