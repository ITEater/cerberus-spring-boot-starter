package top.codef.secure.cloud.components;

import java.util.Collection;
import java.util.List;

import top.codef.secure.authority.AuthWithRequestUrl;
import top.codef.secure.cloud.entities.AuthInfoWithServiceInterface;
import top.codef.secure.cloud.entities.CodefRequestInterface;
import top.codef.secure.cloud.feign.CodefCloudFeign;
import top.codef.secure.requesturl.RequestUrl;
import top.codef.secure.requesturl.interfaces.RequestUrlPersistHandler;

import static java.util.stream.Collectors.*;

public class CodefCloudComponent {

	private final CodefCloudFeign codefCloudFeign;

	private final RequestUrlPersistHandler requestUrlPersistHandler;

	public CodefCloudComponent(CodefCloudFeign codefCloudFeign, RequestUrlPersistHandler requestUrlPersistHandler) {
		this.codefCloudFeign = codefCloudFeign;
		this.requestUrlPersistHandler = requestUrlPersistHandler;
	}

	public void syncRequestInterfaces() {
		Collection<RequestUrl> urls = requestUrlPersistHandler.getCurrentRequestUrl();
		var list = urls.stream().map(x -> CodefRequestInterface.create(x)).flatMap(x -> x.stream()).collect(toList());
		codefCloudFeign.syncInterface(list);
	}

	public List<AuthWithRequestUrl> allDetails() {
		List<AuthInfoWithServiceInterface> list = codefCloudFeign.allDetails();
		List<AuthWithRequestUrl> authWithRequestUrls = list.stream().map(x -> new AuthWithRequestUrl(x))
				.collect(toList());
		return authWithRequestUrls;
	}
}
