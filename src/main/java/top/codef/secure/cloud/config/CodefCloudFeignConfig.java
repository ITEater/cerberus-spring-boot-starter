package top.codef.secure.cloud.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import top.codef.secure.cloud.components.CodefCloudComponent;
import top.codef.secure.cloud.feign.CodefCloudFeign;
import top.codef.secure.properties.CerberusCodefCloudProperties;
import top.codef.secure.requesturl.interfaces.RequestUrlPersistHandler;

@Configuration
@EnableConfigurationProperties({ CerberusCodefCloudProperties.class })
@ConditionalOnProperty(prefix = "cerberus.codef", name = "enabled", havingValue = "true")
@EnableFeignClients(basePackageClasses = { CodefCloudFeign.class })
public class CodefCloudFeignConfig {

	@Bean
	public CodefCloudComponent codefCloudComponent(CodefCloudFeign codefCloudFeign,
			RequestUrlPersistHandler requestUrlPersistHandler,
			CerberusCodefCloudProperties cerberusCodefCloudProperties) {
		CodefCloudComponent component = new CodefCloudComponent(codefCloudFeign, requestUrlPersistHandler);
		return component;
	}
}
