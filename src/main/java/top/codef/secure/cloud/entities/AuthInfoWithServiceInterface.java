package top.codef.secure.cloud.entities;

import java.util.List;

import top.codef.secure.requesturl.RequestUrl;

public class AuthInfoWithServiceInterface extends CodefMightAuth {

	private static final long serialVersionUID = 1L;

	private List<RequestUrl> interfaceList;

	public List<RequestUrl> getInterfaceList() {
		return interfaceList;
	}

	public void setInterfaceList(List<RequestUrl> interfaceList) {
		this.interfaceList = interfaceList;
	}
}
