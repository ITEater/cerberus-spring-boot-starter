package top.codef.secure.cloud.entities;

import top.codef.secure.authority.MightyAuth;

public class CodefMightAuth implements MightyAuth {

	private static final long serialVersionUID = 1L;

	protected String authName;

	protected String uid;

	protected Long pid;

	protected Long id;

	protected Integer showRank;

	protected String componentDetail;

	public String getAuthName() {
		return authName;
	}

	public void setAuthName(String authName) {
		this.authName = authName;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getShowRank() {
		return showRank;
	}

	public void setShowRank(Integer showRank) {
		this.showRank = showRank;
	}

	public String getComponentDetail() {
		return componentDetail;
	}

	public void setComponentDetail(String componentDetail) {
		this.componentDetail = componentDetail;
	}

}
