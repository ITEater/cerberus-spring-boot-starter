package top.codef.secure.cloud.entities;

import java.util.List;

import static java.util.stream.Collectors.*;

import org.springframework.web.bind.annotation.RequestMethod;

import top.codef.secure.requesturl.RequestUrl;

public class CodefRequestInterface {

	private String urlExplain;

	private String serviceUriPattern;

	private RequestMethod httpMethod;

	public static List<CodefRequestInterface> create(RequestUrl requestUrl) {
		var methods = requestUrl.getUrlPattern().getHttpMethods();
		if (methods != null && methods.size() > 0) {
			return methods.stream().map(x -> new CodefRequestInterface(requestUrl.getUrlExplain(),
					requestUrl.getUrlPattern().getServiceUriPattern(), x)).collect(toList());
		} else
			return List.of(new CodefRequestInterface(requestUrl.getUrlExplain(),
					requestUrl.getUrlPattern().getServiceUriPattern(), null));
	}

	public CodefRequestInterface(String urlExplain, String serviceUriPattern, RequestMethod httpMethod) {
		this.urlExplain = urlExplain;
		this.serviceUriPattern = serviceUriPattern;
		this.httpMethod = httpMethod;
	}

	public CodefRequestInterface() {
		super();
	}

	public String getServiceUriPattern() {
		return serviceUriPattern;
	}

	public void setServiceUriPattern(String serviceUriPattern) {
		this.serviceUriPattern = serviceUriPattern;
	}

	public RequestMethod getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(RequestMethod httpMethod) {
		this.httpMethod = httpMethod;
	}

	public String getUrlExplain() {
		return urlExplain;
	}

	public void setUrlExplain(String urlExplain) {
		this.urlExplain = urlExplain;
	}

}
