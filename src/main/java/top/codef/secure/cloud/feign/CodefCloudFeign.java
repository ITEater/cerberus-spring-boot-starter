package top.codef.secure.cloud.feign;

import java.util.Collection;
import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import top.codef.secure.cloud.config.CodefCloudSecureConfig;
import top.codef.secure.cloud.entities.AuthInfoWithServiceInterface;
import top.codef.secure.cloud.entities.CodefRequestInterface;

@FeignClient(name = "cerberus-codef-feign", url = "${cerberus.codef.url}", configuration = CodefCloudSecureConfig.class)
public interface CodefCloudFeign {

	@PostMapping("/auth/serviceInterfaces/synchronizeUrl")
	public void syncInterface(@RequestBody Collection<CodefRequestInterface> requestInterface);

	@PostMapping("/auth/authInterfaceBinding/allDetails")
	public List<AuthInfoWithServiceInterface> allDetails();
}
