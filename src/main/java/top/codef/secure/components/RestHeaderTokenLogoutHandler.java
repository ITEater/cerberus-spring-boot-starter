package top.codef.secure.components;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class RestHeaderTokenLogoutHandler implements LogoutHandler {

	private RedisSecurityContextRepository redisSecurityContextRepository;

	public RestHeaderTokenLogoutHandler(RedisSecurityContextRepository redisSecurityContextRepository) {
		this.redisSecurityContextRepository = redisSecurityContextRepository;
	}

	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		redisSecurityContextRepository.clearContext(request, SecurityContextHolder.getContext());
	}

}
