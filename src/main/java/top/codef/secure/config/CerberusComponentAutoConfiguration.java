package top.codef.secure.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import top.codef.secure.config.interfaces.LoginFailedDecorator;
import top.codef.secure.config.interfaces.LoginSuccessDecorator;
import top.codef.secure.config.interfaces.LogoutSuccessDecorator;

@Configuration
public class CerberusComponentAutoConfiguration {

	private static final Log logger = LogFactory.getLog(CerberusComponentAutoConfiguration.class);

	@Bean
	@ConditionalOnMissingBean
	LoginFailedDecorator loginFailedDecorator() {
		logger.debug("默认登录失败处理");
		return (request, response, exception) -> {
			logger.info("登录失败", exception);
			return null;
		};
	}

	@Bean
	@ConditionalOnMissingBean
	LoginSuccessDecorator loginSuccessDecorator() {
		logger.debug("默认登录成功处理");
		return (request, response, authentication) -> {
			logger.info("登录成功" + authentication.getName());
			return null;
		};
	}

	@Bean
	@ConditionalOnMissingBean
	LogoutSuccessDecorator logoutSuccessDecorator() {
		logger.debug("默认登出处理");
		return (request, response, authentication) -> {
			logger.info("登出成功" + authentication);
			return null;
		};
	}
}
