package top.codef.secure.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import top.codef.secure.properties.CerberusUrlPersisProperties;
import top.codef.secure.requesturl.interfaces.RequestUrlPersistHandler;
import top.codef.secure.requesturl.persist.RedisRequestUrlPersisHandler;

@Configuration
@ConditionalOnClass({ StringRedisTemplate.class })
@ConditionalOnProperty(name = "cerberus.secure.url-persist.enabled", havingValue = "true")
public class CerberusDefaultUrlPersistHandlerConfig {

	@Bean
	@ConditionalOnMissingBean
	public RequestUrlPersistHandler requestUrlPersistHandler(StringRedisTemplate stringRedisTemplate,
			ObjectMapper objectMapper, CerberusUrlPersisProperties cerberusUrlPersisProperties) {
		return new RedisRequestUrlPersisHandler(stringRedisTemplate, objectMapper, cerberusUrlPersisProperties);
	}

}
