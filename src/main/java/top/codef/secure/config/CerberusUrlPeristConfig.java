package top.codef.secure.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import top.codef.secure.properties.CerberusUrlPersisProperties;
import top.codef.secure.requesturl.PersistOnStartComponent;
import top.codef.secure.requesturl.interfaces.RequestUrlExtractor;
import top.codef.secure.requesturl.interfaces.RequestUrlPersistHandler;

@Configuration
@EnableConfigurationProperties(CerberusUrlPersisProperties.class)
@ConditionalOnProperty(name = "cerberus.secure.url-persist.enabled", havingValue = "true")
public class CerberusUrlPeristConfig {

	@Autowired
	private CerberusUrlPersisProperties cerberusUrlPersisProperties;

	private final Log logger = LogFactory.getLog(getClass());

	@Bean
	@ConditionalOnMissingBean
	@ConditionalOnProperty(name = "cerberus.secure.url-persist.upgrade-on-start", havingValue = "true", matchIfMissing = true)
	public PersistOnStartComponent persistOnStartComponent(RequestUrlExtractor requestUrlExtractor,
			RequestUrlPersistHandler requestUrlPersistHandler) {
		logger.debug("开启工程启动自动更新：" + cerberusUrlPersisProperties.getProjectName());
		PersistOnStartComponent persistOnStartComponent = new PersistOnStartComponent(requestUrlPersistHandler,
				requestUrlExtractor);
		return persistOnStartComponent;
	}

}
