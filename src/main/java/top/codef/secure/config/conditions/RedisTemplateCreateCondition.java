package top.codef.secure.config.conditions;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class RedisTemplateCreateCondition implements Condition {

	@Override
	public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
		return "true"
				.equals(context.getEnvironment().getProperty("cerberus.secure.session.enable-redis-session-storage"))
				|| "true".equals(context.getEnvironment().getProperty("cerberus.secure.rest-header.enabled"))
				|| "true".equals(context.getEnvironment().getProperty("cerberus.secure.redis.enabled"));
	}

}
