package top.codef.secure.config.interfaces;

import top.codef.secure.login.CerberusCustomAuthenticationConfigure;

@FunctionalInterface
public interface CerberusCustomLoginCustomer {

	public void custom(CerberusCustomAuthenticationConfigure authenticationConfigure);
}
