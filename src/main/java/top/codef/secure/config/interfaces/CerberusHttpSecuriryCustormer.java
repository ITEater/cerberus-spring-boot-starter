package top.codef.secure.config.interfaces;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;

@FunctionalInterface
public interface CerberusHttpSecuriryCustormer {

	public void custom(HttpSecurity httpSecurity) throws Exception;
}
