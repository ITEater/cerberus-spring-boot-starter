package top.codef.secure.config.interfaces;

import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

@FunctionalInterface
@Order(100)
public interface CerberusHttpSecurityConfig {

	public void secure(HttpSecurity httpSecurity) throws Exception;
}
