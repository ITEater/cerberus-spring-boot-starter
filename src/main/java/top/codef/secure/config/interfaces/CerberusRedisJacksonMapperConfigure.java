package top.codef.secure.config.interfaces;

import com.fasterxml.jackson.databind.ObjectMapper;

@FunctionalInterface
public interface CerberusRedisJacksonMapperConfigure {

	public void configure(ObjectMapper objectMapper);
}
