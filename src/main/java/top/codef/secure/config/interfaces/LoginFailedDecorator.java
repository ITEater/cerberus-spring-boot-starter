package top.codef.secure.config.interfaces;

import org.springframework.security.core.AuthenticationException;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@FunctionalInterface
public interface LoginFailedDecorator {

	public Object decorate(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception);
}
