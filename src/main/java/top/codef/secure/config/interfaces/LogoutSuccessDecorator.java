package top.codef.secure.config.interfaces;

import org.springframework.security.core.Authentication;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@FunctionalInterface
public interface LogoutSuccessDecorator {

	public Object decorate(HttpServletRequest request, HttpServletResponse response, Authentication authentication);
}
