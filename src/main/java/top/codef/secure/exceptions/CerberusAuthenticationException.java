package top.codef.secure.exceptions;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.SpringSecurityCoreVersion;

public class CerberusAuthenticationException extends AuthenticationException {

	private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

	public CerberusAuthenticationException(String msg, Throwable t) {
		super(msg, t);
	}

	public CerberusAuthenticationException(String msg) {
		super(msg);
	}

}
