package top.codef.secure.interfaces;

import org.springframework.security.core.Authentication;

import jakarta.servlet.http.HttpServletRequest;

public interface CaptchaRepository {

	/**
	 * 验证 captcha是否通过
	 * 
	 * @param request
	 * @return
	 */
	public boolean check(HttpServletRequest request);

	/**
	 * 清除现有已通过验证
	 * 
	 * @param authentication
	 */
	public void clear(Authentication authentication);
}
