package top.codef.secure.login;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractAuthenticationFilterConfigurer;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import top.codef.secure.properties.CerberusCustomLoginProperties;

public class CerberusCustomAuthenticationConfigure extends
		AbstractAuthenticationFilterConfigurer<HttpSecurity, CerberusCustomAuthenticationConfigure, CerberusLoginFilter> {

	private final CerberusCustomLoginProperties cerberusCustomLoginProperties;

	private final CerberusLoginFilter cerberusLoginFilter;


	public CerberusCustomAuthenticationConfigure(CerberusCustomLoginProperties cerberusCustomLoginProperties) {
		this.cerberusLoginFilter = new CerberusLoginFilter(cerberusCustomLoginProperties);
		setAuthenticationFilter(cerberusLoginFilter);
		this.cerberusCustomLoginProperties = cerberusCustomLoginProperties;
	}

	@Override
	protected RequestMatcher createLoginProcessingUrlMatcher(String loginProcessingUrl) {
		return new AntPathRequestMatcher(loginProcessingUrl, "POST");
	}

	public CerberusCustomLoginProperties getCerberusCustomLoginProperties() {
		return cerberusCustomLoginProperties;
	}

}
