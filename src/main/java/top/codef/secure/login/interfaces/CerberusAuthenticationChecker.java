package top.codef.secure.login.interfaces;

import org.springframework.security.core.userdetails.UserDetails;

import top.codef.secure.login.CerberusAuthenticationToken;

public interface CerberusAuthenticationChecker extends CerberusOptionalSelector {

	/**
	 * 
	 * 检查用户是否合法
	 * 
	 * @param userDetails 用户信息
	 * @param authentication 待验证token
	 * @return
	 */
	boolean check(UserDetails userDetails, CerberusAuthenticationToken authentication);

}
