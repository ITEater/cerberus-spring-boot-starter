package top.codef.secure.login.interfaces;

import java.util.List;

@FunctionalInterface
public interface CerberusOptionalSelector {

	/**
	 * 
	 * 表示支持的optional参数列表
	 * 
	 * @return
	 */
	public List<String> names();
}
