package top.codef.secure.login.interfaces;

import java.util.Map;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface CerberusUserDetailsService extends CerberusOptionalSelector {

	/**
	 * 通过username获取用户信息
	 * 
	 * @param username 用户信息
	 * @param map 表示需要的header相关
	 * @return
	 * @throws UsernameNotFoundException
	 */
	UserDetails loadUserByUsername(String username, Map<String, ?> map) throws UsernameNotFoundException;
}
