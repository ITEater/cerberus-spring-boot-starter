package top.codef.secure.pojomodel;

import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

public enum SecureResponseEnum {

	AUTH_FAIL(HttpStatus.UNAUTHORIZED, 1, "验证失败"), ACCESS_DENIED(HttpStatus.FORBIDDEN, 3, "拒绝被访问"),
	LOGIN_FAIL(HttpStatus.UNAUTHORIZED, 2, "登录失败"), LOGIN_SUCCESS(HttpStatus.OK, 0, "登录成功"),
	LOGOUT_SUCCESS(HttpStatus.OK, 0, "登出成功"), LOGIN_CAPTCHA_FAILED(HttpStatus.PRECONDITION_FAILED, 10, "人机验证失败");

	private SecureResponseEnum(HttpStatus httpStatus, Integer statusCode, String message) {
		this.httpStatus = httpStatus;
		this.message = message;
		this.statusCode = statusCode;
	}

	private final HttpStatus httpStatus;

	private final String message;

	private final Integer statusCode;

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public String getMessage() {
		return message;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public SecureResultModel<?> createSecureModel() {
		return createSecureModel(statusCode, message);
	}

	public SecureResultModel<?> createSecureModel(String message) {
		return createSecureModel(statusCode, message);
	}

	public SecureResultModel<?> createSecureModel(int statusCode, String message) {
		SecureResultModel<?> resultModel = new SecureResultModel<>(statusCode,
				StringUtils.hasText(message) ? message : this.message);
		return resultModel;
	}

	public SecureResultModel<?> createSecureModel(int statusCode, String message, Object result) {
		SecureResultModel<?> resultModel = new SecureResultModel<>(statusCode,
				StringUtils.hasText(message) ? message : this.message, result);
		return resultModel;
	}
}
