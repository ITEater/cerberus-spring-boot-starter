package top.codef.secure.pojomodel;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_NULL)
public class SecureResultModel<T> {

	private String timestamp = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);

	private int status;

	private String message;

	private T result;

	public SecureResultModel(int status, String message) {
		this.status = status;
		this.message = message;
	}

	public SecureResultModel(int status, String message, T result) {
		this.status = status;
		this.message = message;
		this.result = result;
	}

	public SecureResultModel() {
	}

	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "SecureResultModel [timestamp=" + timestamp + ", status=" + status + ", message=" + message + ", result="
				+ result + "]";
	}

}
