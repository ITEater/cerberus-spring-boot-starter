package top.codef.secure.properties;

import java.util.Arrays;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "cerberus.secure.captcha")
public class CerberusCaptchaProperties {

	/**
	 * 是否开启人机验证码，若是开启，必须实现CaptchaRepository接口
	 */
	private boolean enabled = false;

	/**
	 * 需要人机验证的地址
	 */
	private String[] captchaUris = new String[] {};

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String[] getCaptchaUris() {
		return captchaUris;
	}

	public void setCaptchaUris(String[] captchaUris) {
		this.captchaUris = captchaUris;
	}

	@Override
	public String toString() {
		return "CerberusCaptchaProperties [enabled=" + enabled + ", captchaUris=" + Arrays.toString(captchaUris) + "]";
	}

}
