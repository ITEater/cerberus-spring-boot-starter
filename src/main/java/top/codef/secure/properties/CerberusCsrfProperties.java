package top.codef.secure.properties;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "cerberus.secure.csrf")
public class CerberusCsrfProperties {

	/**
	 * 是否开启scrf
	 */
	private boolean enabled = false;

	/**
	 * csrfToken存储在header中的键名
	 */
	private String csrfHeaderName = "X-Token";

	/**
	 * csrfToken存储在Session中的名称
	 */
	private String csrfSessionAttrName = "CSRF.TOKEN";

	private List<String> ignoringPathMatchers = new ArrayList<>(0);

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getCsrfHeaderName() {
		return csrfHeaderName;
	}

	public void setCsrfHeaderName(String csrfHeaderName) {
		this.csrfHeaderName = csrfHeaderName;
	}

	public String getCsrfSessionAttrName() {
		return csrfSessionAttrName;
	}

	public void setCsrfSessionAttrName(String csrfSessionAttrName) {
		this.csrfSessionAttrName = csrfSessionAttrName;
	}

	public List<String> getIgnoringPathMatchers() {
		return ignoringPathMatchers;
	}

	public void setIgnoringPathMatchers(List<String> ignoringPathMatchers) {
		this.ignoringPathMatchers = ignoringPathMatchers;
	}

}
