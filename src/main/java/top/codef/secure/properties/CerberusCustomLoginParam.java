package top.codef.secure.properties;

import java.util.Collections;
import java.util.List;

public class CerberusCustomLoginParam {

	private String principalParam = "username";

	private String credentialParam = "password";

	private List<String> headerParamKeys = Collections.emptyList();

	public String getPrincipalParam() {
		return principalParam;
	}

	public void setPrincipalParam(String principalParam) {
		this.principalParam = principalParam;
	}

	public String getCredentialParam() {
		return credentialParam;
	}

	public void setCredentialParam(String credentialParam) {
		this.credentialParam = credentialParam;
	}

	public List<String> getHeaderParamKeys() {
		return headerParamKeys;
	}

	public void setHeaderParamKeys(List<String> headerParamKeys) {
		this.headerParamKeys = headerParamKeys;
	}

	@Override
	public String toString() {
		return "CerberusCustomLoginParam [principalParam=" + principalParam + ", credentialParam=" + credentialParam
				+ ", headerParamKeys=" + headerParamKeys + "]";
	}
}
