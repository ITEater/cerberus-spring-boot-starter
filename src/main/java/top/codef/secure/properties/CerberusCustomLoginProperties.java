package top.codef.secure.properties;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "cerberus.secure.custom-login")
public class CerberusCustomLoginProperties {

	private boolean enabled = false;

	private String loginPath = "/login";

	private String optionParam = "optional";

	private Map<String, CerberusCustomLoginParam> params;

	private boolean restful = true;

	public Map<String, CerberusCustomLoginParam> getParams() {
		return params;
	}

	public void setParams(Map<String, CerberusCustomLoginParam> params) {
		this.params = params;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getLoginPath() {
		return loginPath;
	}

	public void setLoginPath(String loginPath) {
		this.loginPath = loginPath;
	}

	public String getOptionParam() {
		return optionParam;
	}

	public void setOptionParam(String optionParam) {
		this.optionParam = optionParam;
	}

	public boolean isRestful() {
		return restful;
	}

	public void setRestful(boolean restful) {
		this.restful = restful;
	}

	@Override
	public String toString() {
		return "CerberusCustomLoginProperties [enabled=" + enabled + ", loginPath=" + loginPath + ", optionParam="
				+ optionParam + ", params=" + params + ", restful=" + restful + "]";
	}

}
