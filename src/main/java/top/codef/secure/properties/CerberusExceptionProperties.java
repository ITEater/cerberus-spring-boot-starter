package top.codef.secure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpStatus;

@ConfigurationProperties(prefix = "cerberus.secure.exception")
public class CerberusExceptionProperties {

	private boolean enabled = true;

	private HttpStatus missingCsrfToken = HttpStatus.FORBIDDEN;

	private HttpStatus InvalidateCsrfToken = HttpStatus.FORBIDDEN;

	private HttpStatus NotAuthorize = HttpStatus.FORBIDDEN;

	private HttpStatus otherAccessDenied = HttpStatus.FORBIDDEN;

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public HttpStatus getMissingCsrfToken() {
		return missingCsrfToken;
	}

	public void setMissingCsrfToken(HttpStatus missingCsrfToken) {
		this.missingCsrfToken = missingCsrfToken;
	}

	public HttpStatus getInvalidateCsrfToken() {
		return InvalidateCsrfToken;
	}

	public void setInvalidateCsrfToken(HttpStatus invalidateCsrfToken) {
		InvalidateCsrfToken = invalidateCsrfToken;
	}

	public HttpStatus getNotAuthorize() {
		return NotAuthorize;
	}

	public void setNotAuthorize(HttpStatus notAuthorize) {
		NotAuthorize = notAuthorize;
	}

	public HttpStatus getOtherAccessDenied() {
		return otherAccessDenied;
	}

	public void setOtherAccessDenied(HttpStatus otherAccessDenied) {
		this.otherAccessDenied = otherAccessDenied;
	}

}
