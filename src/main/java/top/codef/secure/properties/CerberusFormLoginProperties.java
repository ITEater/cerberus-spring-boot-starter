package top.codef.secure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpStatus;

@ConfigurationProperties(prefix = "cerberus.form-login")
public class CerberusFormLoginProperties {

	private boolean restful = false;

	private String paramUsername = "username";

	private String paramPwd = "password";

	private String LoginPath = "/login";

	private boolean enabled = false;

	private HttpStatus failStatus = HttpStatus.UNAUTHORIZED;

	private boolean changeCsrfTokenOnLoginSuccess = false;

	/**
	 * @return the restful
	 */
	public boolean isRestful() {
		return restful;
	}

	/**
	 * @param restful the restful to set
	 */
	public void setRestful(boolean restful) {
		this.restful = restful;
	}

	public String getLoginPath() {
		return LoginPath;
	}

	public void setLoginPath(String loginPath) {
		LoginPath = loginPath;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getParamUsername() {
		return paramUsername;
	}

	public void setParamUsername(String paramUsername) {
		this.paramUsername = paramUsername;
	}

	public String getParamPwd() {
		return paramPwd;
	}

	public void setParamPwd(String paramPwd) {
		this.paramPwd = paramPwd;
	}

	public HttpStatus getFailStatus() {
		return failStatus;
	}

	public void setFailStatus(HttpStatus failStatus) {
		this.failStatus = failStatus;
	}

	public boolean isChangeCsrfTokenOnLoginSuccess() {
		return changeCsrfTokenOnLoginSuccess;
	}

	public void setChangeCsrfTokenOnLoginSuccess(boolean changeCsrfTokenOnLoginSuccess) {
		this.changeCsrfTokenOnLoginSuccess = changeCsrfTokenOnLoginSuccess;
	}
}
