package top.codef.secure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpStatus;

@ConfigurationProperties(prefix = "cerberus.secure.login-return")
public class CerberusLoginReturnProperties {

	private String successMessage = "登录成功";

	private Integer successMessageStatus = 0;

	private HttpStatus successHttpStatus = HttpStatus.OK;

	private String failedMessage = "登录失败";

	private Integer failedMessageStatus = 1;

	private HttpStatus failedHttpStatus = HttpStatus.UNAUTHORIZED;

	public String getSuccessMessage() {
		return successMessage;
	}

	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}

	public Integer getSuccessMessageStatus() {
		return successMessageStatus;
	}

	public void setSuccessMessageStatus(Integer successMessageStatus) {
		this.successMessageStatus = successMessageStatus;
	}

	public HttpStatus getSuccessHttpStatus() {
		return successHttpStatus;
	}

	public void setSuccessHttpStatus(HttpStatus successHttpStatus) {
		this.successHttpStatus = successHttpStatus;
	}

	public String getFailedMessage() {
		return failedMessage;
	}

	public void setFailedMessage(String failedMessage) {
		this.failedMessage = failedMessage;
	}

	public Integer getFailedMessageStatus() {
		return failedMessageStatus;
	}

	public void setFailedMessageStatus(Integer failedMessageStatus) {
		this.failedMessageStatus = failedMessageStatus;
	}

	public HttpStatus getFailedHttpStatus() {
		return failedHttpStatus;
	}

	public void setFailedHttpStatus(HttpStatus failedHttpStatus) {
		this.failedHttpStatus = failedHttpStatus;
	}

}
