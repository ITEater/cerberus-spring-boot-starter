package top.codef.secure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpStatus;

@ConfigurationProperties(prefix = "cerberus.secure.logout")
public class CerberusLogoutProperties {

//	private boolean enabled = true;

	private boolean clearCookie = false;

	private String logoutPath = "/logout";

	private String successMessage = "登录成功";

	private Integer successMessageStatus = 0;

	private HttpStatus successHttpStatus = HttpStatus.OK;

//	public boolean isEnabled() {
//		return enabled;
//	}
//
//	public void setEnabled(boolean enabled) {
//		this.enabled = enabled;
//	}

	public boolean isClearCookie() {
		return clearCookie;
	}

	/**
	 * @return the successMessage
	 */
	public String getSuccessMessage() {
		return successMessage;
	}

	/**
	 * @param successMessage the successMessage to set
	 */
	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}

	/**
	 * @return the successMessageStatus
	 */
	public Integer getSuccessMessageStatus() {
		return successMessageStatus;
	}

	/**
	 * @param successMessageStatus the successMessageStatus to set
	 */
	public void setSuccessMessageStatus(Integer successMessageStatus) {
		this.successMessageStatus = successMessageStatus;
	}

	/**
	 * @return the successHttpStatus
	 */
	public HttpStatus getSuccessHttpStatus() {
		return successHttpStatus;
	}

	/**
	 * @param successHttpStatus the successHttpStatus to set
	 */
	public void setSuccessHttpStatus(HttpStatus successHttpStatus) {
		this.successHttpStatus = successHttpStatus;
	}

	public void setClearCookie(boolean clearCookie) {
		this.clearCookie = clearCookie;
	}

	public String getLogoutPath() {
		return logoutPath;
	}

	public void setLogoutPath(String logoutPath) {
		this.logoutPath = logoutPath;
	}

}
