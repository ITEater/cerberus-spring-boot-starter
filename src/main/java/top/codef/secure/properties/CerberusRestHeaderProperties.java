package top.codef.secure.properties;

import java.time.Duration;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "cerberus.secure.rest-header")
public class CerberusRestHeaderProperties {

	private boolean enabled = false;

	private String headerTokenKey = "X-Token";

	private boolean enableSso = false;

	private String redisKeyPrefix = "cerberus:rest_header";

	private Duration timeout = Duration.ofHours(2);

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getHeaderTokenKey() {
		return headerTokenKey;
	}

	public void setHeaderTokenKey(String headerTokenKey) {
		this.headerTokenKey = headerTokenKey;
	}

	public boolean isEnableSso() {
		return enableSso;
	}

	public void setEnableSso(boolean enableSso) {
		this.enableSso = enableSso;
	}

	public Duration getTimeout() {
		return timeout;
	}

	public void setTimeout(Duration timeout) {
		this.timeout = timeout;
	}

	public String getRedisKeyPrefix() {
		return redisKeyPrefix;
	}

	public void setRedisKeyPrefix(String redisKeyPrefix) {
		this.redisKeyPrefix = redisKeyPrefix;
	}

}
