package top.codef.secure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.security.config.http.SessionCreationPolicy;

import top.codef.secure.properties.enums.InvalidateSessionHandle;

@ConfigurationProperties(prefix = "cerberus.secure.session")
public class CerberusSessionProperties {

	private boolean enabled = false;

	private InvalidateSessionHandle invalidateSessionHandle = InvalidateSessionHandle.AUTH_FAILED;

	private String invalidateSessionRedirectUrl = "/";

	private int maxSession;

	private boolean enableRedisSessionStorage;

	private SessionCreationPolicy sessionCreationPolicy = SessionCreationPolicy.IF_REQUIRED;

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public InvalidateSessionHandle getInvalidateSessionHandle() {
		return invalidateSessionHandle;
	}

	public void setInvalidateSessionHandle(InvalidateSessionHandle invalidateSessionHandle) {
		this.invalidateSessionHandle = invalidateSessionHandle;
	}

	public String getInvalidateSessionRedirectUrl() {
		return invalidateSessionRedirectUrl;
	}

	public void setInvalidateSessionRedirectUrl(String invalidateSessionRedirectUrl) {
		this.invalidateSessionRedirectUrl = invalidateSessionRedirectUrl;
	}

	public int getMaxSession() {
		return maxSession;
	}

	public void setMaxSession(int maxSession) {
		this.maxSession = maxSession;
	}

	public boolean isEnableRedisSessionStorage() {
		return enableRedisSessionStorage;
	}

	public void setEnableRedisSessionStorage(boolean enableRedisSessionStorage) {
		this.enableRedisSessionStorage = enableRedisSessionStorage;
	}

	public SessionCreationPolicy getSessionCreationPolicy() {
		return sessionCreationPolicy;
	}

	public void setSessionCreationPolicy(SessionCreationPolicy sessionCreationPolicy) {
		this.sessionCreationPolicy = sessionCreationPolicy;
	}

}
