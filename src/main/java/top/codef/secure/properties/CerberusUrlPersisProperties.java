package top.codef.secure.properties;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "cerberus.secure.url-persist")
public class CerberusUrlPersisProperties {

	private boolean enabled;

	/**
	 * 是否是工程启动时就进行更新
	 */
	private boolean upgradeOnStart = true;

	/**
	 * redis键的前缀，redis存储request的键组成为prefix+工程名
	 */
	private String redisKeyPrefix = "cerberus:request_url";

	/**
	 * 工程名：默认的以spring.project.name替代
	 */
	@Value("${cerberus.secure.request.redis.project-name:${spring.application.name:project}}")
	private String projectName;

	/**
	 * 不用进行存储的url
	 */
	private List<String> excludeUrl;

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isUpgradeOnStart() {
		return upgradeOnStart;
	}

	public void setUpgradeOnStart(boolean upgradeOnStart) {
		this.upgradeOnStart = upgradeOnStart;
	}

	public String getRedisKeyPrefix() {
		return redisKeyPrefix;
	}

	public void setRedisKeyPrefix(String redisKeyPrefix) {
		this.redisKeyPrefix = redisKeyPrefix;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public List<String> getExcludeUrl() {
		return excludeUrl;
	}

	public void setExcludeUrl(List<String> excludeUrl) {
		this.excludeUrl = excludeUrl;
	}

}
