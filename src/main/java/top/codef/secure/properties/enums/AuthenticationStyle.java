package top.codef.secure.properties.enums;

public enum AuthenticationStyle {

	SESSION, HEADER, PARAM
}
