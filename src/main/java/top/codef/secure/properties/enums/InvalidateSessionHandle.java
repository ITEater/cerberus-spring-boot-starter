package top.codef.secure.properties.enums;

public enum InvalidateSessionHandle {

	NO_HANDLE, REDIRECT, FORBIDDEN, AUTH_FAILED;
}
