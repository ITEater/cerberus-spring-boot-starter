package top.codef.secure.properties.enums;

public enum RequestStorageType {

	REDIS, SQL, MICRO_SERVICE;
}
