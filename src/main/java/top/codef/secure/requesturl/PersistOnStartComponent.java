package top.codef.secure.requesturl;

import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import top.codef.secure.requesturl.interfaces.RequestUrlExtractor;
import top.codef.secure.requesturl.interfaces.RequestUrlPersistHandler;

public class PersistOnStartComponent implements InitializingBean {

	private final RequestUrlPersistHandler requestUrlPersistHandler;

	private final RequestUrlExtractor requestUrlExtractor;

	public PersistOnStartComponent(RequestUrlPersistHandler requestUrlPersistHandler,
			RequestUrlExtractor requestUrlExtractor) {
		this.requestUrlPersistHandler = requestUrlPersistHandler;
		this.requestUrlExtractor = requestUrlExtractor;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		requestUrlPersistHandler.clear();
		List<RequestUrl> list = requestUrlExtractor.extract();
		if (list.size() == 0)
			return;
		requestUrlPersistHandler.save(list);
	}

}
