package top.codef.secure.requesturl;

public class RequestUrl {

	private UrlPattern urlPattern;

	private String urlExplain;

	public RequestUrl(UrlPattern urlPattern, String urlExplain) {
		this.urlPattern = urlPattern;
		this.urlExplain = urlExplain;
	}

	public RequestUrl() {
	}

	public UrlPattern getUrlPattern() {
		return urlPattern;
	}

	public void setUrlPattern(UrlPattern urlPattern) {
		this.urlPattern = urlPattern;
	}

	public String getUrlExplain() {
		return urlExplain;
	}

	public void setUrlExplain(String urlExplain) {
		this.urlExplain = urlExplain;
	}

	@Override
	public String toString() {
		return "RequestUrl [urlPattern=" + urlPattern + ", urlExplain=" + urlExplain + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((urlPattern == null) ? 0 : urlPattern.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RequestUrl other = (RequestUrl) obj;
		if (urlPattern == null) {
			if (other.urlPattern != null)
				return false;
		} else if (!urlPattern.equals(other.urlPattern))
			return false;
		return true;
	}

	
	
}
