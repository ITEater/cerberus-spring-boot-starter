package top.codef.secure.requesturl;

import java.util.Set;

import org.springframework.web.bind.annotation.RequestMethod;

public class UrlPattern {

	private String serviceUriPattern;

	private Set<RequestMethod> httpMethods;

	public UrlPattern() {
	}

	public UrlPattern(String serviceUriPattern, Set<RequestMethod> httpMethods) {
		this.serviceUriPattern = serviceUriPattern;
		this.httpMethods = httpMethods;
	}

	public String getServiceUriPattern() {
		return serviceUriPattern;
	}

	public void setServiceUriPattern(String serviceUriPattern) {
		this.serviceUriPattern = serviceUriPattern;
	}

	public Set<RequestMethod> getHttpMethods() {
		return httpMethods;
	}

	public void setHttpMethods(Set<RequestMethod> httpMethods) {
		this.httpMethods = httpMethods;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((httpMethods == null) ? 0 : httpMethods.hashCode());
		result = prime * result + ((serviceUriPattern == null) ? 0 : serviceUriPattern.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UrlPattern other = (UrlPattern) obj;
		if (httpMethods != other.httpMethods)
			return false;
		if (serviceUriPattern == null) {
			if (other.serviceUriPattern != null)
				return false;
		} else if (!serviceUriPattern.equals(other.serviceUriPattern))
			return false;
		return true;
	}

}
