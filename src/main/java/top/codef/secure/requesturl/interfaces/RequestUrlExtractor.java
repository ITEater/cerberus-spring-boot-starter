package top.codef.secure.requesturl.interfaces;

import java.util.List;

import top.codef.secure.requesturl.RequestUrl;

@FunctionalInterface
public interface RequestUrlExtractor {

	public List<RequestUrl> extract();
}
