package top.codef.secure.requesturl.interfaces;

import java.util.Collection;
import java.util.List;

import top.codef.secure.requesturl.RequestUrl;

public interface RequestUrlPersistHandler {

	public void clear();

	public void save(List<RequestUrl> list);

	public Collection<RequestUrl> getCurrentRequestUrl();
}
